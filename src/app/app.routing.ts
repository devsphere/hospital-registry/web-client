import { Routes, RouterModule } from "@angular/router";
import {HomeComponent} from "./component/home/home.component";
import {LoginComponent} from "./component/login/login.component";
import {AccountComponent} from "./component/account/account.component";
import {StaffComponent} from "./component/staff/staff.component";
import {ManagerComponent} from "./component/manager/manager.component";

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent},
  { path: 'account', component: AccountComponent},
  { path: 'staff', component: StaffComponent},
  { path: 'management', component: ManagerComponent},
  { path: '**', redirectTo: '' }
]

export const appRoutingModule = RouterModule.forRoot(routes);
