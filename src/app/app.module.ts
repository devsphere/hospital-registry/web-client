import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {RouterLinkWithHref, RouterOutlet} from "@angular/router";
import {authInterceptorProviders} from "./util/auth-interceptor";
import {appRoutingModule} from "./app.routing";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { AccountComponent } from './component/account/account.component';
import { StaffComponent } from './component/staff/staff.component';
import { ManagerComponent } from './component/manager/manager.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AccountComponent,
    StaffComponent,
    ManagerComponent
  ],
  imports: [
    BrowserModule,
    appRoutingModule,
    HttpClientModule,
    RouterOutlet,
    RouterLinkWithHref,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
