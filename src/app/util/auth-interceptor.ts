import {HTTP_INTERCEPTORS, HttpEvent, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {HttpInterceptor, HttpHandler, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenStorage} from "../service/auth/token-storage.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private jwt: string | null;
  private tokenStorage: TokenStorage;

  constructor(tokenStorage: TokenStorage) {
    this.jwt = "";
    this.tokenStorage = tokenStorage;
  }

  intercept(outgoingRequest: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    this.jwt = this.tokenStorage.getToken();
    const request = outgoingRequest.clone({
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': '' + this.jwt
      })
    });
    console.log(this.jwt + " | " + request.headers.get('Authorization'));
    return httpHandler.handle(request);
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
