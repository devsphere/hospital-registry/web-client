import {Person} from "./Person";
import {Policy} from "./Policy";
import {Employment} from "./Employment";

export interface Patient extends Person {
  employment: Employment;
  policy: Policy;
}
