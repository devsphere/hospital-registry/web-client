import {Department} from "./Department";

export interface Manager {
  employmentDate: Date;
  department: Department;
}
