import {Person} from "./Person";

export interface Doctor extends Person {
  employmentDate: Date;
  specialization: string;
  cabinet: number
}
