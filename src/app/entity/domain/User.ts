import {Person} from "./Person";

export interface User {
  role: string;
  person: Person;
}
