import {Doctor} from "./Doctor";
import {Patient} from "./Patient";
import {Time} from "@angular/common";

export interface Referral {
  doctor: Doctor;
  patient: Patient;
  appointmentDate: Date;
  appointmentTime: Time;
}
