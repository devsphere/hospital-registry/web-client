export enum Employment {
  UNKNOWN,
  STUDENT,
  EMPLOYED,
  UNEMPLOYED
}
