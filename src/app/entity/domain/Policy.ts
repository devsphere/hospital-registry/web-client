export interface Policy {
  id: number;
  code: string;
  registrationDate: Date;
  expirationDate: Date
}
