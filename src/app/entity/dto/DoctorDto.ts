export interface DoctorDto {
  name: string,
  lastname: string,
  email: string,
  birthDate: string,
  employmentDate: string,
  specialization: string,
  cabinet: number
}
