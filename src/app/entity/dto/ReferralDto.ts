export interface ReferralDto {
  doctorEmail: string;
  patientEmail: string;
  date: string;
  time: string;
}
