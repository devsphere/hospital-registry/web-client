export interface PatientDto {
  name: string,
  lastname: string,
  email: string,
  birthDate: string,
  employment: string
}
