import { Component } from '@angular/core';
import {TokenStorage} from "./service/auth/token-storage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private tokenStorage: TokenStorage;
  user: any;
  isAuthenticated: boolean = false;
  isPatient: boolean = false;
  isAdmin: boolean = false;

  hospitalName = 'Loutsker\'s Hospital'
  contactEmail = 'com.javapadawan@gmail.com';

  constructor(tokenStorage: TokenStorage) {
    this.tokenStorage = tokenStorage;
    this.user = tokenStorage.getUser();
    if (this.user != null) {
      this.isAuthenticated = true;
      if (this.user.role == "PATIENT") {
        this.isPatient = true;
      }
      if (this.user.role == "ADMIN") {
        this.isAdmin = true;
      }
    }
  }
}
