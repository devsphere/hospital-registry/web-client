import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {DoctorDto} from "../../entity/dto/DoctorDto";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private webBffAddress: string;
  private httpOptions: {headers: HttpHeaders, body: any};
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      body: null
    };
    this.webBffAddress = environment.webBFFUrl + environment.webBFFPort + "/web/ui/domain/doctor";
  }

  public getAllDoctors(): Observable<any> {
    return this.http.get(
      this.webBffAddress + "/all",
      this.httpOptions
    );
  }

  deleteDoctorByEmail(email: string): Observable<any> {
    return this.http.delete(
      this.webBffAddress + "/delete/" + email,
      this.httpOptions
    )
  }

  addDoctor(doctor: DoctorDto): Observable<any> {
    return this.http.post(
      this.webBffAddress + "/save",
      doctor,
      this.httpOptions
    );
  }
}
