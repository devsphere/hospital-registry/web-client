import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ReferralDto} from "../../entity/dto/ReferralDto";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ReferralService {
  private webBffAddress: string;
  private httpOptions: {headers: HttpHeaders};
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    this.webBffAddress = environment.webBFFUrl + environment.webBFFPort + "/web/ui/domain/referral";
  }

  public getReferralsOnPatientEmail(email: string): Observable<any> {
    return this.http.get(
      this.webBffAddress + "/onPatient/" + email,
      this.httpOptions
    );
  }

  public getReferralsOnDoctorEmail(email: string): Observable<any> {
    return this.http.get(
      this.webBffAddress + "/onDoctor/" + email,
      this.httpOptions
    );
  }

  public saveReferral(dto: ReferralDto): Observable<any> {
    return this.http.post(
      this.webBffAddress + "/save",
      dto,
      this.httpOptions
    );
  }

  public deleteReferral(dto: ReferralDto): Observable<any> {
    const options = {
      headers: this.httpOptions.headers,
      body: dto
    };
    return this.http.delete(
      this.webBffAddress + '/delete',
      options
    );
  }
}
