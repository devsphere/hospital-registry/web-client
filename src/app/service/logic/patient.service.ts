import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {PatientDto} from "../../entity/dto/PatientDto";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private webBffAddress: string;
  private httpOptions: {headers: HttpHeaders, body: any};
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      body: null
    };
    this.webBffAddress = environment.webBFFUrl + environment.webBFFPort + "/web/ui/domain/patient";
  }

  addPatient(dto: PatientDto): Observable<any> {
    return this.http.post(
      this.webBffAddress + "/save",
      dto,
      this.httpOptions
    );
  }
}
