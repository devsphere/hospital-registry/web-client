import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private webBffAddress: string;
  private httpOptions: {headers: HttpHeaders};
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.webBffAddress = environment.webBFFUrl + environment.webBFFPort + "/web/ui";
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    this.http = http;
  }

  login(email: string, password: string): Observable<any> {
    return this.http.post(
      this.webBffAddress + "/security/authentication",
      {
        email,
        password
      },
      this.httpOptions
    );
  }
}
