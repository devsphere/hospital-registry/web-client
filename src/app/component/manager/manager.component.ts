import { Component, OnInit } from '@angular/core';
import {DoctorService} from "../../service/logic/doctor.service";
import {Doctor} from "../../entity/domain/Doctor";
import {FormControl, FormGroup} from "@angular/forms";
import {DoctorDto} from "../../entity/dto/DoctorDto";
import {ReferralService} from "../../service/logic/referral.service";
import {Referral} from "../../entity/domain/Referral";
import {PatientDto} from "../../entity/dto/PatientDto";
import {error} from "@angular/compiler-cli/src/transformers/util";
import {PatientService} from "../../service/logic/patient.service";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  private doctorService: DoctorService;
  private referralService: ReferralService;
  private patientService: PatientService;
  doctorList: Doctor[] = [];
  doctorsToDelete: Doctor[] = [];
  referralsOnDoctor: Referral[] = [];

  doctorForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    specialization: new FormControl(''),
    birthDate: new FormControl(''),
    cabinet: new FormControl('')
  });

  patientForm: FormGroup = new FormGroup({
    name: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    birthDate: new FormControl(''),
    employment: new FormControl('')
  });

  refsOnDoctorForm: FormGroup = new FormGroup({
    targetEmail: new FormControl('')
  });

  constructor(doctorService: DoctorService, referralService: ReferralService,
              patientService: PatientService) {
    this.doctorService = doctorService;
    this.referralService = referralService;
    this.patientService = patientService;
  }

  ngOnInit(): void {
    this.downloadDoctors();
  }

  public addPatient(): void {
    const dto = this.getPatientDtoFromForm();
    this.patientService.addPatient(dto).subscribe(
      next => {
        alert('Saved');

      }, error => {
        alert('Denied: \n' + error.error.message);
      }
    );
  }

  public getReferralsOnDoctor(): void {
    let email = this.refsOnDoctorForm.controls['targetEmail'].value;
    this.referralService.getReferralsOnDoctorEmail(email).subscribe(
      data => {
        this.referralsOnDoctor = data;
      },
      error => {
        alert('Enable to fetch referrals: ' + error.error.message);
      }
    )
  }

  public addDoctor(): void {
    const dto = this.getDoctorDtoFromForm();
    this.doctorService.addDoctor(dto).subscribe(
      success => {
        alert('Saved');
      },
      error => {
        alert('Denied: \n' + error.error.message);
      }, () => {
        this.reloadPage();
      }
    );
  }

  public onCheckChange(value: Doctor): void {
    if (this.doctorsToDelete.includes(value)) {
      this.doctorsToDelete.splice(this.doctorsToDelete.indexOf(value));
    } else {
      this.doctorsToDelete.push(value);
    }
  }

  public deleteSelectedDoctors(): void {
    let length = this.doctorsToDelete.length;
    for (let j = 0; j < length; j++) {
      let doctor = this.doctorsToDelete[j];
      let email = doctor.email;
      this.doctorService.deleteDoctorByEmail(email).subscribe(
        success => {
          alert('Deleted')
        }, error => {
          alert('Denied: \n' + error.error.message);
        }, () => {
          this.reloadPage();
          this.doctorsToDelete = [];
      }
      );
    }
  }

  private getPatientDtoFromForm(): PatientDto {
    return <PatientDto> {
      name: <string> this.patientForm.controls['name'].value,
      lastname: <string> this.patientForm.controls['lastname'].value,
      email: <string> this.patientForm.controls['email'].value,
      birthDate: <string> this.patientForm.controls['birthDate'].value,
      employment: <string> this.patientForm.controls['employment'].value
    };
  }

  private getDoctorDtoFromForm(): DoctorDto {
    return <DoctorDto> {
      name: <string> this.doctorForm.controls['name'].value,
      lastname: <string> this.doctorForm.controls['lastname'].value,
      email: <string> this.doctorForm.controls['email'].value,
      birthDate: <string> this.doctorForm.controls['birthDate'].value,
      employmentDate: <string> (new Date()).toString(),
      specialization: <string> this.doctorForm.controls['specialization'].value,
      cabinet: this.doctorForm.controls['cabinet'].value
    };
  }

  private downloadDoctors(): void {
    this.doctorService.getAllDoctors().subscribe(
      data => {
        this.doctorList = data;
      },
      error => {
        alert("Enable to fetch doctors: " + error.error.message);
      }
    )
  }

  private reloadPage(): void {
    window.location.reload();
  }
}
