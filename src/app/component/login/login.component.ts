import { Component, OnInit } from '@angular/core';
import {TokenStorage} from "../../service/auth/token-storage.service";
import {AuthenticationService} from "../../service/auth/authentication.service";
import {FormControl, FormGroup} from "@angular/forms";
import {User} from "../../entity/domain/User";
import {environment} from "../../../environments/environment.prod";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private tokenStorage: TokenStorage;
  private authService: AuthenticationService;

  loginForm = new FormGroup({
    email: new FormControl('email'),
    password: new FormControl('password')
  });

  isLoggedIn: boolean = false;
  isLoginFailed: boolean = false;
  errorMessage: string = "";

  constructor(tokenStorage: TokenStorage, authService: AuthenticationService) {
    this.tokenStorage = tokenStorage;
    this.authService = authService;
  }

  ngOnInit(): void {
    const jwt = this.tokenStorage.getToken();
    if (jwt != null) {
      this.isLoggedIn = true;
    }
  }

  public login(): void {
    let email = <string> this.loginForm.value.email;
    let password = <string> this.loginForm.value.password;

    this.authService.login(email, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        const user = this.getUserFromDto(data);
        this.tokenStorage.saveUser(user);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.reloadPage();
      },
      error => {
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
    )
  }

  public signOut(): void {
    this.tokenStorage.signOut();
    this.reloadPage();
  }

  private reloadPage(): void {
    window.location.reload();
  }

  private getUserFromDto(dto: any): User {
    return {
      role: dto.role,
      person: dto.person
    }
  }
}
