import {Component, OnInit} from '@angular/core';
import {Doctor} from "../../entity/domain/Doctor";
import {DoctorService} from "../../service/logic/doctor.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {
  private doctorService: DoctorService;
  doctorList: Doctor[];

  constructor(doctorService: DoctorService) {
    this.doctorService = doctorService;
  }

  ngOnInit(): void {
    this.downloadDoctors();
  }

  private downloadDoctors(): void {
    this.doctorService.getAllDoctors().subscribe(
      data => {
        this.doctorList = data;
      },
      error => {
        alert("Enable to fetch doctors: " + error.error.message);
      }
    )
  }

  getExperience(employmentDate: Date): string {
    let employmentYear = new Date(employmentDate).getFullYear();
    let currentYear = new Date().getFullYear();
    let experience =  Math.ceil(currentYear - employmentYear);
    if (experience < 1) {
      return 'trainee';
    } else if (experience == 1) {
      return experience + ' year'
    } else {
      return experience + ' years';
    }
  }
}
