import { Component, OnInit } from '@angular/core';
import {TokenStorage} from "../../service/auth/token-storage.service";
import {ReferralService} from "../../service/logic/referral.service";
import {FormControl, FormGroup} from "@angular/forms";
import {DoctorService} from "../../service/logic/doctor.service";
import {Doctor} from "../../entity/domain/Doctor";
import {ReferralDto} from "../../entity/dto/ReferralDto";
import {Referral} from "../../entity/domain/Referral";
import {Patient} from "../../entity/domain/Patient";
import {assertSuccessfulReferenceEmit} from "@angular/compiler-cli/src/ngtsc/imports";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  private tokenService: TokenStorage;
  private referralService: ReferralService;
  private doctorService: DoctorService;

  patient!: Patient;
  doctorList: Doctor[] = [];
  referralList: Referral[] = [];
  refsToDelete: Referral[] = [];

  referralForm: FormGroup = new FormGroup({
    email: new FormControl(''),
    date: new FormControl(''),
    time: new FormControl('')
  });

  constructor(tokenService: TokenStorage, referralService: ReferralService,
              doctorService: DoctorService) {
    this.referralService = referralService;
    this.tokenService = tokenService;
    this.doctorService = doctorService;
  }

  ngOnInit(): void {
    this.downloadDoctors();
    this.downloadReferrals();
    this.setPatient();
  }

  public deleteSelectedReferrals(): void {
    let length = this.refsToDelete.length;
    for (let j = 0; j < length; j++) {
      let referral = this.refsToDelete[j];
      let dto = this.getDtoFromReferral(referral);
      this.referralService.deleteReferral(dto).subscribe(
        () => {
          alert('Deleted');
          this.refsToDelete = [];
          this.reloadPage();
        },
        error => {
          alert('Denied: \n' + error.error.message);
        }
      )
    }
  }

  public onCheckChange(value: Referral): void {
    if (this.refsToDelete.includes(value)) {
      this.refsToDelete.splice(this.refsToDelete.indexOf(value));
    } else {
      this.refsToDelete.push(value);
    }
  }

  public addReferral(): void {
    let dto = this.getReferralDtoFromForm();
    this.referralService.saveReferral(dto).subscribe(
      data => {
        alert('Appointed');
        this.reloadPage();
      },
      error => {
        alert('Denied: \n' + error.error.message);
      }
    );
  }

  private getDtoFromReferral(referral: Referral): ReferralDto {
    let doctorEmail = referral.doctor.email;
    let patientEmail = referral.patient.email;
    let date = referral.appointmentDate.toString();
    let time = <string><unknown> referral.appointmentTime;
    return <ReferralDto> {
      doctorEmail: doctorEmail,
      patientEmail: patientEmail,
      date: date,
      time: time
    };
  }

  private getReferralDtoFromForm(): ReferralDto {
    let patientEmail = this.tokenService.getUser()?.person.email;
    let doctorEmail = <string> this.referralForm.controls['email'].value;
    let date = <string> this.referralForm.controls['date'].value;
    let time = <string> this.referralForm.controls['time'].value;
    return <ReferralDto> {
      doctorEmail: doctorEmail,
      patientEmail: patientEmail,
      date: date,
      time: time
    }
  }

  private downloadReferrals(): void {
    let email = <string> this.tokenService.getUser()?.person.email;
    this.referralService.getReferralsOnPatientEmail(email).subscribe(
      data => {
        this.referralList = data;
      },
      error => {
        alert("Enable to fetch referrals on user: " + error.error.message);
      }
    )
  }

  private downloadDoctors(): void {
    this.doctorService.getAllDoctors().subscribe(
      data => {
        this.doctorList = data;
      },
      error => {
        alert("Enable to fetch doctors: " + error.error.message);
      }
    )
  }

  private setPatient(): void {
    this.patient = <Patient> this.tokenService.getUser()?.person;
  }

  private reloadPage(): void {
    window.location.reload();
  }
}
