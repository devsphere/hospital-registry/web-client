(function(window) {
  window.env = window.env || {};
  window["env"]["webBFFUrl"] = "${WEB_BFF_URL}";
  window["env"]["webBFFPort"] = "${WEB_BFF_PORT}";
})(this);
