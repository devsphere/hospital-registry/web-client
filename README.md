# Web Client

This is a microsevice. Used as component of Hospital Registry application with basic hospital registry functionality.
<br/>
The microsevice provides web client for the application.

Running application link: http://194.31.174.36:9099/
<br/>
DockerHub - https://hub.docker.com/repositories/rloutsker

## Application architecture
![Application architecture](https://gitlab.com/devsphere/hospital-registry/authentication-server/-/raw/master/HospitalRegistry-SystemArchitecture.png)

